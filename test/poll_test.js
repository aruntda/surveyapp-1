var expect = require('chai').expect;
var mongo = require('mongodb');
var monk = require('monk');
var request = require('supertest');


describe('poll-test', function () {
    var url = "http://localhost:3000";
    var db;
    var questions;
    var _id = -1;
    var first_choice_id = 0;
    beforeEach(function (done) {
        db = monk('localhost:27017/pollsdb');
        questions = db.get("PollSchema");
        done();
    });

    after(function (done) {
        questions.remove({'question': "what is test?"});
        done();
    });

    it('should pass this chai canary test', function () {
        expect(true).to.be.true;
    });

    it("test connection to DB", function (done) {

        questions.insert({question: "what is test?"}, function (err, doc) {
            if (err) {
                return done(err);
            }
            var expectedFirstQuestion = "what is test?"
            expect(expectedFirstQuestion).to.equal(doc.question);
            done();
        });
    });


    it('should post question with choices', function (done) {
        var body = {
            "question":"whats next 4?",
            "choices":[
                {"text":"I am fined 4"},
                {"text":"I am good 4"},
                {"text":"Not good 4"}
            ]
        };
        request(url)
            .post('/poll')
            .send(body)
            .expect('Content-Type', /json/)
            .expect(200) //Status code
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                expect(res.body._id).to.equal(_id);
                expect(res.body.question).to.equal("whats next 4?");
                expect(res.body.choices.length).to.equal(3);
                expect(res.body.choices[0].text).to.equal("I am fined 4");
                expect(res.body.choices[1].text).to.equal("I am good 4");
                expect(res.body.choices[2].text).to.equal("Not good 4");
                done();
            });
    });

    it('should get correct numbered questions given a question number or id', function (done) {

        request(url)
            .get('/poll/'+_id)
            .expect(200) //Status code
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                expect(res.body[0]._id).to.equal(_id);
                expect(res.body[0].question).to.equal("whats next 4?");
                expect(res.body[0].choices.length).to.equal(3);
                expect(res.body[0].choices[0].text).to.equal("I am fined 4");
                first_choice_id = res.body[0].choices[0]._id;
                expect(res.body[0].choices[1].text).to.equal("I am good 4");
                expect(res.body[0].choices[2].text).to.equal("Not good 4");
                done();
            });
    });

    it('should correctly vote for an existing choice', function (done) {
        var body = {
            choice: first_choice_id
        };
        request(url)
            .put('/poll/' + _id)
            .send(body)
            .expect('Content-Type', /json/)
            .expect(200) //Status code
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                expect(res.body[0].choices[0].votes.length).to.greaterThan(0);
                done();
            });
    });

    it('question should not be in DB after deletion', function (done) {
        request(url)
            .del('/poll/'+ _id)
            .expect(200) //Status code
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                var question = questions.find({'question':'whats next 4?'});
                expect(question.question).to.equal(undefined);
                done();
            });
    });

});